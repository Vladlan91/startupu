@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div class="row">
    <div class="row placeholders">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <img src = "/uploads/avatars/{{$team->avatar}}" style="width:150px;height:150px;float: left;border-radius: 50%;margin-right: 25px; ">
            <h2>{{$team-> title}}</h2>
        </div>
    </div>
</div>
@endsection