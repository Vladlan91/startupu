<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function author()
    {
        return $this->belongsTo('App\User','author_id');
    }

    public function team()
    {
        return $this->belongsTo('App\Team','projects_id');
    }

}
