<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
use App\Town;

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
// главная страница
Route::get('/home', 'HomeController@index');
Route::get('/projects', 'ProjectController@index');
Route::get('project/{id}','ProjectController@show')->where('id', '[0-9]+');
Route::get('edit/{id}','ProjectController@edit')->where('id', '[0-9]+');
Route::post('update-project','ProjectController@update');
Route::post('update-team','TeamController@update');
Route::get('/new-project','ProjectController@create');
Route::post('/new-project','ProjectController@store');
Route::get('delete-project/{id}','ProjectController@destroy');
Route::get('users', 'UserController@show_users');
Route::get('edit-team/{slug}','TeamController@edit')->where('id', '[0-9]+');
Route::get('teams', 'TeamController@show_teams');
Route::get('team-destroy/{id}','TeamController@destroy');
Route::get('team/{id}', 'TeamController@show_team');
Route::get('/new-team','TeamController@create');
Route::post('/new-team','TeamController@store');
Route::get('edit-team/{slug}','TeamController@edit');
Route::get('/new-post', 'PostController@create');
Route::get('my-all-posts','UserController@user_posts_all');
Route::get('my-drafts','UserController@user_posts_draft');
Route::get('profile', 'UserController@profile');
Route::post('profile', 'UserController@update_avatar');
Route::get('auth/{provider}', 'Auth\RegisterController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback');
Route::get('/{slug}',['as' => 'post', 'uses' => 'PostController@show']);
Route::get('user/{id}','UserController@profiles')->where('id', '[0-9]+');
Route::get('user/{id}/posts','UserController@user_posts')->where('id', '[0-9]+');
Route::get('my-all-posts','UserController@user_posts_all');
Route::get('my-drafts','UserController@user_posts_draft');
Route::post('comment/delete/{id}','CommentController@distroy');
Route::get('edit/{slug}','PostController@edit');
Route::post('update','PostController@update');
Route::get('delete/{id}','PostController@destroy');
Route::post('comment/add','CommentController@store');
Route::post('/new-post','PostController@store');
Route::get('/new-post','PostController@create');








