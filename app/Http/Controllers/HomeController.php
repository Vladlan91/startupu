<?php

namespace App\Http\Controllers;
use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $posts = Post::where('active',1)->orderBy('created_at','desc')->paginate(100);
        $title = 'Последние посты';

        return view('home')->withPosts($posts)->withTitle($title);
    }

}
