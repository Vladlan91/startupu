<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    function socialProviders()
    {
        return $this->hasMany(SocialProvider::class);
    }

    public function posts()
    {
        return $this->hasMany('App\Post','author_id');
    }
    public function projects()
    {
        return $this->hasMany('App\Project','author_id');
    }
    public function own_teams()
    {
        return $this->hasMany('App\Team','own_id');
    }
    // у пользователя много комментариев
    public function comments()
    {
        return $this->hasMany('App\Comment','from_user');
    }

    public function teams()
    {
        return $this->belongsToMany('App\Team','team_groups','user_id','team_id');
    }

    public function jobes()
    {
        return $this->belongsToMany('App\Job', 'job_users', 'user_id', 'job_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }
    public function town()
    {
        return $this->belongsTo('App\Town');
    }


    public function can_post()
    {
        $role = $this->role;
        if($role == 'author' || $role == 'admin')
        {
            return true;
        }
        return false;
    }
    public function is_admin()
    {
        $role = $this->role;
        if($role == 'admin')
        {
            return true;
        }
        return false;
    }
}
