<?php

use Illuminate\Database\Seeder;
use App\Town;

class TownsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Town::create(['name'=>'Не вказано','country_id'=>'2']);
        Town::create(['name'=>'Вінниця','country_id'=>'2']);
        Town::create(['name'=>'Київ','country_id'=>'2']);
        Town::create(['name'=>'Франківсь','country_id'=>'2']);
        Town::create(['name'=>'Харків','country_id'=>'2']);


    }
}
