<?php

use Illuminate\Database\Seeder;
use App\Team;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Team::create(['avatar'=>'team1.jpg','own_id'=>'1','title'=>'Hardcore','body'=>'1234','slug'=>'hard-core',]);
        Team::create(['avatar'=>'team2.jpg','own_id'=>'2','title'=>'Roony','body'=>'1234','slug'=>'roony',]);
        Team::create(['avatar'=>'team3.jpg','own_id'=>'2','title'=>'Bigup','body'=>'1234','slug'=>'bigup',]);
        Team::create(['avatar'=>'team4.jpg','own_id'=>'1','title'=>'Roly','body'=>'1234','slug'=>'roly',]);
        Team::create(['avatar'=>'team5.jpg','own_id'=>'1','title'=>'WeTheBast','body'=>'1234','slug'=>'we-the-bast',]);
    }
}
