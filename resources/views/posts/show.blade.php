@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div class="row">
    <div class="row placeholders">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <p></p>>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        @if($post)
        <div class="row">
            <div class="row placeholders">
                <div class="col-md-8 col-md-offset-2">
                    <div class="list-group">
                        <div class="list-group-item">
                            <h3>{{ $post->title }}
                                @if(!Auth::guest() && ($post->author_id == Auth::user()->id || Auth::user()->is_admin()))
                                @if($post->active == '1')
                                <button class="btn" style="float: right; background-color: #111c26"><a href="{{ url('edit/'.$post->slug)}}">Редактировать пост</a></button>
                                @else
                                <button class="btn" style="float: right; background-color: #111c26"><a href="{{ url('edit/'.$post->slug)}}">Редактировать черновик</a></button>
                                @endif
                                @endif
                            </h3>
                            <img src = "/uploads/avatars/{{$post->author->avatar}}" style="width:30px;height:30px;float: left;border-radius: 50%;margin-right: 25px; ">
                            <p>{{ $post->created_at->format('M d,Y \a\t h:i a') }} By <a href="{{ url('/user/'.$post->author_id)}}">{{ $post->author->name }}</a></p>
                        </div>
                        <div style="width: 100%; height: 400px;">
                            <img src = "/uploads/avatars/{{$post->avatar}}" style="width:100%;height:400px;margin-right: 25px; ">
                        </div>
                        <div class="list-group-item">
                            <article>
                                {!! $post->body !!}
                            </article>
                        </div>
                        <div class="list-group">
                            <a href="#" class="list-group-item">  <span><i class="pe-7s-timer"></i></span> Просмотрено <span class="badge">9</span>
                            </a>
                            <a href="#" class="list-group-item"> <span><i class="pe-7s-timer"></i></span> Коментарив <span class="badge">24</span>
                            </a>
                            <a href="#" class="list-group-item"> <span><i class="pe-7s-timer"></i></span> Лайков <span class="badge">411</span>

                            </a>
                        </div>
                    </div>
            @endif
            <div>
                <h2>Оставить комментарий</h2>
            </div>
            @if(Auth::guest())
            <p>Залогинтесь, чтоб комментрировать</p>
            @else
            <div class="panel-body">
                <form method="post" action="/comment/add"">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="on_post" value="{{ $post->id }}">
                        <input type="hidden" name="slug" value="{{ $post->slug }}">
                    <div class="form-group">
                        <textarea required="required" placeholder="Введите свой комментарий" name = "body" class="form-control"></textarea>
                    </div>
                        <input type="submit" name='post_comment' class="btn btn-success" value = "Опубликовать"/>
                </form>
            </div>
            @endif
            <div>
                @if($comments)
                <ul style="list-style: none; padding: 0">
                    @foreach($comments as $comment)
                    <li class="panel-body">
                        <div class="list-group">
                            <div class="list-group-item">
                                <h3>{{ $comment->author->name }}</h3>
                                <p>{{ $comment->created_at->format('M d,Y \a\t h:i a') }}</p>
                            </div>
                            <div class="list-group-item">
                                <p>{{ $comment->body }}</p>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
                @endif
            </div>
        </div>
    </div>
    </div>
    </div>
</div>
</div>

