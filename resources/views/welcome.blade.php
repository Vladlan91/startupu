<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>STARTUP-UKRAINE</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Raleway:500,600,700,800,900,400,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Owl Carousel Assets -->
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <link rel="stylesheet" href="css/all.css">
    <!-- Pixeden Icon Font -->
    <link rel="stylesheet" href="css/Pe-icon-7-stroke.css">
    <!-- Font Awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <!-- PrettyPhoto -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <!-- Style -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/rok.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
    <!-- Styles -->
    <style>


        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }
    </style>
</head>
<body>
    <!-- PRELOADER -->
    <div class="spn_hol">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

 <!-- END PRELOADER -->

 <!-- =========================
     START ABOUT US SECTION
============================== -->
    <section class="header parallax home-parallax page" id="HOME">
        <h2></h2>
        <div class="section_overlay">
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img src="images/logo.png" alt="Logo">
                        </a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <!-- NAV -->
                             @if (Route::has('login'))
                              @if (Auth::check())
                            <li><a href="{{ url('/home') }}">ГОЛОВНА</a></li>
                            <li><a href="#FEATURES">ПРО НАС</a> </li>
                            <li><a href="#ABOUT">ПРО ПРОДУКТ</a> </li>
                            <li><a href="#CONTACT">КОНТАКТИ</a> </li>
                            @else
                            <li><a href="#ABOUT">ПРО ПРОДУКТ</a> </li>
                            <li><a href="#FEATURES">ПРО НАС</a> </li>
                            <li><a href="#CONTACT">КОНТАКТИ</a> </li>
                            <li><a href="{{ url('/login') }}">УВІЙТИ</a> </li>
                            <li><a href="{{ url('/register') }}">ЗАРЕЄСТРУВАТИСЯ</a> </li>
                            @endif
                            @endif
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container- -->
            </nav>

            <div class="container home-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo text-center">
                                <!-- LOGO -->
                            <img width="525" height="85" src="images/logoStart.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <div class="home_text">
                            <!-- TITLE AND DESC -->
                            
                            <h1>МИ ДОПОМОЖЕМО ЗНАЙТИ ВІДПОВІДЬ НА ВСІ ЦІ ТА ІНШІ ЗАПИТАННЯ.</h1>
                            <p>Де знайти ідею, і що робити, якщо вона не спрацювала? Який продукт виводити на ринок? Як скласти бізнес-план і чи потрібен він взагалі? Де знайти команду?</p>

                            <div class="download-btn">
                            <!-- BUTTON -->
                                <a class="btn home-btn wow fadeInLeft" href="{{ url('/register') }}">ЗАРЕЄСТРУВАТИСЯ</a>
                                <a class="tuor btn wow fadeInRight" href="#ABOUT">Дізнатись більше<i class="fa fa-angle-down"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-1 col-sm-4">
                        <div class="home-iphone">
                            <div id="rocket"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<body>

     <section class="about page" id="ABOUT">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- ABOUT US SECTION TITLE-->
                     <div class="inner_about_title">
                        <h2>STARTUP UKRAINE</h2>
                        <p>STARTUP UKRAINE – це інтерактивна навчальна платформа, яка була побудована на основі бізнес акселератора. Процес навчання проходить в командах від 7 до 10 учасників і на основі реальних проектів. Ця платформа для тих, хто хоче створити стартап з високим потенціалом зростання.</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="inner_about_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
                        <!-- PHONE -->
                            <img src="images/one.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6  wow fadeInRight" data-wow-duration="1s" data-wow-delay=".5s">
                        <!-- TITLE -->
                        <div class="inner_about_title">
                            <h2>ЩО ВИ ОТРИМАЄТЕ ПО ЗАВЕРШЕНІ  <br> НАВЧАЛЬНОЇ   ПРОГРАМИ?</h2>
                            <p>Кожний учасник програми отримає для себе масу нових можливостей, які безумовно будуть корисні в майбутньому.</p>
                        </div>
                        <div class="inner_about_desc">

                            <!-- SINGLE DESC -->
                            <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="1s">
                                <!-- ICON -->
                                <div><i class="pe-7s-timer"></i></div>
                                <!-- HEADING DESCRIPTION -->
                                <h3>ФІЛОСОФІЯ LEAN STARTUP</h3>
                                <p>Оволодієте навиками  пошуку ідей, формуванням гіпотез, розробкою і тестуванням бізнес-моделі, використовуючи науковий підхід у впровадженні будь-якого нового продукту, послуги або ідеї.</p>
                            </div>
                            <!-- END SINGLE DESC -->


                            <!-- SINGLE DESC -->
                            <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="1.5s">
                                <!-- ICON -->
                                <div><i class="pe-7s-target"></i></div>
                                <!-- HEADING DESCRIPTION -->
                                <h3>КОМАНДА</h3>
                                <p>Знайдете першокласну команду, яка зможе реалізувати будь-яку ідею, не розвалиться після невдалої реалізації, а переключиться на іншу ідею і продовжить шлях до успіху.</p>
                            </div>
                            <!-- END SINGLE DESC -->


                            <!-- SINGLE DESC -->
                            <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="2s">
                                <!-- ICON -->
                                <div><i class="pe-7s-stopwatch"></i></div>
                                <!-- HEADING DESCRIPTION -->
                                <h3>НАВЧАННЯ IT – ТЕХНОЛОГІЯМ</h3>
                                <p>Навики в програмуванні являються не від’ємною частиною любої стартап- команди, це дасть можливість з мінімальними затратами вивести ваш проект в простори інтернету.</p>
                            </div>
                            <!-- END SINGLE DESC -->

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="video_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 wow fadeInLeftBig">
                    <!-- VIDEO LEFT TITLE -->
                        <div class="video_title">
                            <h2>ОГЛЯД ПЛАТФОРМИ <br>STARTUP UKRAINE</h2>
                            <p>Дізнайтесь більше про інтерактивну навчальну платформу STARTUP UKRAINE .</p>
                        </div>
                        <div class="video-button">
                            <!-- BUTTON -->
                            <a class="btn btn-primary btn-video" href="#FEATURES" role="button">Детальніше</a>
                        </div>
                    </div>
                    <div class="col-md-6 wow fadeInRightBig">
                         <!-- VIDEO -->
                        <div class="video">
                            <iframe src="https://player.vimeo.com/video/88209254?title=0&amp;byline=0&amp;portrait=0&amp;color=cc0000" width="560" height="315"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End About Us -->




 <!-- =========================
     START TESTIMONIAL SECTION
============================== -->
 <section id="TESTIMONIAL" class="testimonial parallax">
        <div class="section_overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow bounceInDown">
                        <div id="carousel-example-caption-testimonial" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-caption-testimonial" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-caption-testimonial" data-slide-to="1"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <!-- IMAGE -->
                                                <img src="images/client_2.png" alt="">
                                                <div class="testimonial_caption">
                                                   <!-- DESCRIPTION -->  
                                                    <h2>Dan Harmon</h2>
                                                    <h4><span>SR. UI Designer,</span> Dcrazed</h4>
                                                    <p>“Lorem ipsum dolor sit amet, consectetur adipisicing elit, do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.”</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                 <!-- IMAGE -->
                                                <img src="images/client_1.png" alt="">
                                                <div class="testimonial_caption">
                                                <!-- DESCRIPTION --> 
                                                    <h2>Ivanitskiy Vlad</h2>
                                                    <h4><span>Співзасновник,</span> STARTUP UKRAINE </h4>
                                                    <p>“Ми розробили новий підхід в навчані креативної молоді, взявши за основу модель "Бізнес-акселератор". Ми гарантуємо море позетивних вражень, командний дух і безліч цікавих подій.”</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- END TESTIMONIAL SECTION -->

<section id="FEATURES" class="features page">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- FEATURES SECTION TITLE -->
                    <div class="section_title wow fadeIn" data-wow-duration="1s">
                        <h2>НАША ІСТОРІЯ</h2>
                        <p>В той час як онлайн – освіта стає досить актуальна в світі, її рівень в Україні зовсім низький. Ми впевнені, що майбутнє за інтерактивним навчанням. STARTUP UKRAINE - родився в поєднані ігрової і соціальної складової, а основною ізюминкою стала можливість на базі нашої платформи створити власний стартап, поєднавши тим самим приємне з корисним.  </p>
                    </div>
                    <!-- END FEATURES SECTION TITLE -->
                </div>
            </div>
        </div>
    </section>

        <div class="call_to_action" id="EMAIL">
        <div class="container">
            <div class="row wow fadeInLeftBig" data-wow-duration="1s">
                    <div class="container">
                <div class="row  wow lightSpeedIn">
                    <div class="col-md-6 col-md-offset-3">
                        <!-- SUBSCRIPTION SUCCESSFUL OR ERROR MESSAGES -->
                        <div class="subscription-success"></div>
                        <div class="subscription-error"></div>
                        <p>Для того, щоб підписатися на розсилку новин введіть свою адресу<p>
                        <form id="mc-form" action="https://designscrazed.us8.list-manage.com/subscribe/post" method="POST" class="subscribe_form">                         
                        <input type="hidden" name="u" value="6908378c60c82103f3d7e8f1c">
                        <input type="hidden" name="id" value="8c5074025d">
                            <div class="form-group">
                                <!-- EMAIL INPUT BOX -->
                                <input type="email" autocapitalize="off" autocorrect="off" name="MERGE0" class="required email form-control" id="mce-EMAIL" placeholder="Введіть ваш email" value="" >                                 
                            </div>
                                <!-- SUBSCRIBE BUTTON -->
                            <button type="submit" class="btn btn-default subs-btn">Підписатися</button>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>

<!-- =========================
     START FEATURES
============================== -->
    <section id="FEATURES" class="features page">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- FEATURES SECTION TITLE -->
                    <div class="section_title wow fadeIn" data-wow-duration="1s">
                        <h2>ФУКЦІОНАЛ ПЛАТФОРМИ</h2>
                        <p>Навчайтесь, шукайте команду, тестуйте ідеї і розвивайте ваш стартап користуючись нашою платформою. </p>
                    </div>
                    <!-- END FEATURES SECTION TITLE -->
                </div>
            </div>
        </div>

        <div class="feature_inner">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 right_no_padding wow fadeInLeft" data-wow-duration="1s">
                        <!-- FEATURE -->

                        <div class="left_single_feature">
                            <!-- ICON -->
                            <div><span class="pe-7s-like"></span></div>

                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3>ПОШУК КОМАНДИ<span>/</span></h3>
                            <p>Користувач має власний PROFIT–SKILS, який дає змогу оцінити навики кожного учасника проекту.</p>
                        </div>

                        <!-- END SINGLE FEATURE -->


                        <!-- FEATURE -->
                        <div class="left_single_feature">
                            <!-- ICON -->
                            <div><span class="pe-7s-science"></span></div>

                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3>РОЗВИТОК ЗДІБНОСТЕЙ<span>/</span></h3>
                            <p>Навчаючись на нашій платформі ви пройдете весь шлях від пошуку ідеї до масштабування бізнесу разом з нами. </p>
                        </div>
                        <!-- END SINGLE FEATURE -->


                        <!-- FEATURE -->
                        <div class="left_single_feature">
                            <!-- ICON -->
                            <div><span class="pe-7s-look"></span></div>

                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3>ТЕСТУВАННЯ ІДЕЙ<span>/</span></h3>
                            <p>Всі учасники LEAN STARTUP також виступають в ролі фокус - групи, що дає можливість отримати експертизу на ранніх стадіях.</p>
                        </div>
                        <!-- END SINGLE FEATURE -->

                    </div>
                    <div class="col-md-4">
                        <div class="feature_iphone">
                            <!-- FEATURE PHONE IMAGE -->
                            <img class="wow bounceIn" data-wow-duration="1s" src="/images/screen_lean.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-4 left_no_padding wow fadeInRight" data-wow-duration="1s">

                        <!-- FEATURE -->
                        <div class="right_single_feature">
                            <!-- ICON -->
                            <div><span class="pe-7s-monitor"></span></div>

                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3><span>/</span>ЗАЛУЧЕННЯ ІНВЕСТИЦІЙ</h3>
                            <p>Користуючись нашою платформою LEAN STARTUP вам необхідно займатись тільки  розвитком вашого бізнесу, а інвестори знайдуть вас самі.</p>
                        </div>
                        <!-- END SINGLE FEATURE -->


                        <!-- FEATURE -->
                        <div class="right_single_feature">
                            <!-- ICON -->
                            <div><span class="pe-7s-phone"></span></div>

                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3><span>/</span>ПОШУК МЕНТОРІВ</h3>
                            <p>Ми знайдемо вам наставника,  завданням якого буде супровід та допомога,  щоб ви змогли  довести ваш  проект до успішного завершення.</p>
                        </div>
                        <!-- END SINGLE FEATURE -->


                        <!-- FEATURE -->
                        <div class="right_single_feature">
                            <!-- ICON -->
                            <div><span class="pe-7s-gleam"></span></div>

                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3><span>/</span>МАШТАБУВАННЯ БЫЗНЕСУ</h3>
                            <p>Будуйте бізнес без кордонів, шукаючи команди за межами України, адже і ми  ставимо перед собою таку ж ціль.</p>
                        </div>
                        <!-- END SINGLE FEATURE -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END FEATURES SECTION -->

 <div id="wrapper">
    <section class="area">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <h2 class="visible-xs visible-sm text-primary">ЧОМУ НЕОБХІДНІ НАВИКИ FRONTEND РОЗРОБНИКА?</h2>
                    <ul class="visual-list">
                        <li>
                            <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="1s">
                                <div><i class="pe-7s-user"></i></div>
                               
                                <h3>Перспективно!</h3>
                                <p>Число IT-фахівців в Україні є найбільшим і найбільш швидко зростаючим в Європі. Очікується, що до 2020 року кількість IT-фахівців в країні наблизиться до позначки 200 тисяч.</p>
                            
                            </div>
                        </li>
                        <li>
                         <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="1s">
                              <div><i class="pe-7s-server"></i></div>
                            
                        
                                <h3>Прибутково!</h3>
                                <p>Сумарні інвестиції в IT-стартапи в Україні за останні 5 років перевищили $ 240 млн, що набагато більше, ніж в будь-якій країні Східної або Центральної Європи. </p>
                            </div>
                        </li>
                        <li>
                       <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="1s">
                               <div><i class="pe-7s-speaker"></i></div>
                          
        
                                <h3>Актуально!</h3>
                                <p>Репутація України як країни інвестиційних можливостей падає через загальну кризову ситуацію, проте це не стосується ринку IT-стартапів, котрі  відкривають для інвесторів значні переваги.</p>
                            </div>
                        </li>
                        <li>
                            <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="1s">
                                 <div><i class="pe-7s-study"></i></div>
                           
                          
                                <h3>Майбутнє за IT технологіями!</h3>
                                <p>Близько 65% користувачів підключені до мереж 3G і активно використовують мобільні додатки, більшість будинків підключені до інтернет-зв'язку, відповідно  це і спонукає до розвитку IT – індустрії.  </p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-7">
                    <div class="slide-holder">
                         <div class="inner_about_title">
                        <h2 >ЧОМУ НЕОБХІДНІ НАВИКИ FRONTEND РОЗРОБНИКА?</h2>
                        <div class="img-slide scroll-trigger"><img src="images/img-01.png" height="624" width="1184" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

   <div class="call_to_action" id="SPONSOR"> </div>

    <section class="visual-container" >
        <div class="visual-area">
            <div class="container">
                <h2>МИ ІСНУЄМО ЗАВДЯКИ ПІДТРИМЦІ</h2>
                <ul class="testimonials">
                    <li>
                        <div class="img-holder"><a href="https://vladlan91.github.io/"><img src="images/logo2.png" height="43" width="165" alt="smashing magazine"></a></div>
                        <p><em>Mолода, проте перспективна компанія, метою якої є - конкурентний, прибутковий український бізнес на ринку Європи.</em></p>
                    </li>
                    <li>
                        <div class="img-holder"><a href="#"><img src="images/logo-codrops.png" height="50" width="148" alt="codrops"></a></div>
                       <p><em>IT - компанія</em></p>
                    </li>
                    <li>
                        <div class="img-holder"><a href="#"><img src="images/logo-w.png" height="64" width="64" alt="w"></a></div>
                         <p><em>Рекламне агенство </em></p>  </li>
                   </ul>
               </div>
            <img src="images/img-decor-02.jpg" height="764" width="1380" alt="" class="bg-stretch">
        </div>
        <div class="visual-area">
            <div class="container">
                <h2>ТАРИФНИЙ ПЛАН</h2>
                <div class="pricing-tables">
                    <div class="plan">
                        <div class="head">
                            <h3>Users</h3>
                        </div>
                        <div class="price">
                            <span class="price-main"><span class="symbol">$</span>0</span>
                            <span class="price-additional">в місяць</span>
                        </div>
                        <ul class="item-list">
                            <li>В центрі подій</li>
                        </ul>
                        <button type="button" class="btn btn-default rounded">придбати</button>
                    </div>
                    <div class="plan">
                        <div class="head">
                            <h3>Students</h3> </div>
                        <div class="price">
                            <span class="price-main"><span class="symbol">$</span>5</span>
                            <span class="price-additional">в місяць</span>
                        </div>
                            <ul class="item-list">
                               <li>Навчальна програма</li>
                            </ul>
                        <button type="button" class="btn btn-default rounded">придбати</button>
                    </div>
                    <div class="plan recommended">
                        <div class="head">
                            <h3>Professional</h3> </div>
                        <div class="price">
                            <span class="price-main"><span class="symbol">$</span>10</span>
                            <span class="price-additional">в місяць</span>
                        </div>
                            <ul class="item-list">
                                <li>Пошук команд</li>
                                <li>Створення проектів</li>
                            </ul>
                        <button type="button" class="btn btn-default rounded">придбати</button>
                    </div>
                    <div class="plan">
                        <div class="head">
                            <h3>Enterprise</h3> </div>
                        <div class="price">
                            <span class="price-main"><span class="symbol">$</span>20</span>
                            <span class="price-additional">в місяць</span>
                        </div>
                        <ul class="item-list">
                             <li>Залучення інвестицій</li>
                             <li>Підтримка 7/24</li>
                        </ul>
                        <button type="button" class="btn btn-default rounded">придбати</button>
                    </div>
                </div>
             </div>
            <img src="images/img-decor-03.jpg" height="1175" width="1380" alt="" class="bg-stretch">
        </div>
    </section>
</div>

<!-- =========================
     START CALL TO ACTION
============================== -->
   <div class="call_to_action"> </div>
<!-- =========================
     Start FUN FACTS
============================== -->


<!-- =========================
     Start APPS SCREEN SECTION
============================== -->
    <section class="apps_screen page" id="SCREENS">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 wow fadeInBig" data-wow-duration="1s">
                        <!-- APPS SCREEN TITLE -->
                        <div class="section_title">
                            <h2>ОСНОВНІ ЕТАПИ</h2>
                        </div>
                        <!-- END APPS SCREEN TITLE -->
                    </div>
                </div>
            </div>

        <div class="screen_slider">
            <div id="demo" class="wow bounceInRight" data-wow-duration="1s">
                <div id="owl-demo" class="owl-carousel">

                    <!-- APPS SCREEN IMAGES -->
                    <div class="item">
                    <a href="images/screens/iPhone04.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone04.png" width="60" height="60" alt="APPS SCREEN" /></a>
                   <p>Пошук та генерація ідей</p>
                    </div>
                    <div class="item">
                        <a href="images/screens/iPhone05.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone05.png" width="60" height="60" alt="APPS SCREEN" /></a>
                   <p>Підбір цікавих технологій</p>
                    </div>
                    <div class="item">
                        <a href="images/screens/iPhone06.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone06.png" width="60" height="60" alt="APPS SCREEN" /></a>
                   <p>Проектування прототипу</p>
                    </div>
                    <div class="item">
                        <a href="images/screens/iPhone07.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone07.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    <p>Розвиток та оптимізація</p>
                    </div>
                    <div class="item">
                        <a href="images/screens/iPhone08.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone08.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    <p>Залучення цільового ринку</p>
                    </div>
                    <div class="item">
                        <a href="images/screens/iPhone09.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone09.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    <p>Аналіз та маштабування</p>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- ENS APPS SCREEN -->





<!-- =========================
     Start FUN FACTS
============================== -->



    <section class="fun_facts parallax">
        <div class="section_overlay">
            <div class="container wow bounceInLeft" data-wow-duration="1s">
                <div class="row text-center">
                    <div class="col-md-3">
                        <div class="single_fun_facts">
                            <i class="pe-7s-cloud-download"></i>
                            <h2><span  class="counter_num">699</span> <span>+</span></h2>
                            <p>Підписаних</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="single_fun_facts">
                            <i class="pe-7s-look"></i>
                            <h2><span  class="counter_num">1999</span> <span>+</span></h2>
                            <p>Зацікавлених</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="single_fun_facts">
                            <i class="pe-7s-comment"></i>
                            <h2><span  class="counter_num">199</span> <span>+</span></h2>
                            <p>Учасників</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="single_fun_facts">
                            <i class="pe-7s-cup"></i>
                            <h2><span  class="counter_num">10</span> <span>+</span></h2>
                            <p>Випусників</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- END FUN FACTS -->


<!-- =========================
     START DOWNLOAD NOW 
============================== -->
    <section class="download page" id="DOWNLOAD">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- DOWNLOAD NOW SECTION TITLE -->
                    <div class="section_title">
                        <h2>PROFIT – SKILS</h2>
                        <p>Це зручний інструмент порівняння і пошуку команд, за навиками якими вони володіють. Даний підхід був запозичений з games world, так як ігровий підхід, дозволяє більш ефективніше залучити користувача в процес навчання. Інтерактивна платформа дає можливість навчатися в команді, так як найголовнішим є команда, це більше чим частина бізнес – процесу. </p>
                    </div>
                    <!--END DOWNLOAD NOW SECTION TITLE -->
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="download_screen text-center wow fadeInUp" data-wow-duration="1s">
                        <img src="images/subscribe.png" alt="">
                    </div>
                </div>
            </div>
        </div>

           <div class="call_to_action"> </div>
    </section>
    <!-- END DOWNLOAD -->
<!-- =========================
     START CONTCT FORM AREA
============================== -->
    <section class="contact page" id="CONTACT">
        <div class="section_overlay">
            <div class="container">
                <div class="col-md-10 col-md-offset-1 wow bounceIn">
                    <!-- Start Contact Section Title-->
                    <div class="section_title">
                        <h2>Звяжіться з нами</h2>
                        <p>При виникненні будь яких запитань звертайтесь через зворотній зв'язок, також Ви можете зв'язатись з нами за телефоном, який вказано нижче.</p>
                    </div>
                </div>
            </div>

            <div class="contact_form wow bounceIn">
                <div class="container">

                <!-- START ERROR AND SUCCESS MESSAGE -->
                    <div class="form_error text-center">
                        <div class="name_error hide error">Please Enter your name</div>
                        <div class="email_error hide error">Please Enter your Email</div>
                        <div class="email_val_error hide error">Please Enter a Valid Email Address</div>
                        <div class="message_error hide error">Please Enter Your Message</div>
                    </div>
                    <div class="Sucess"></div>
                <!-- END ERROR AND SUCCESS MESSAGE -->

                <!-- CONTACT FORM starts here, Go to contact.php and add your email ID, thats it.-->    
                    <form role="form" action="contact.php">
                        <div class="row">
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="name" placeholder="Name">
                                <input type="email" class="form-control" id="email" placeholder="Email">
                                <input type="text" class="form-control" id="subject" placeholder="Subject">
                            </div>


                            <div class="col-md-8">
                                <textarea class="form-control" id="message" rows="25" cols="10" placeholder="  Message Texts..."></textarea>
                                <button type="button" class="btn btn-default submit-btn form_submit">НАДІСЛАТИ ПОВІДОМЛЕННЯ</button>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM --> 
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow bounceInLeft">
                        <div class="social_icons">
                            <ul>
                                <li><a href=""><i class="fa fa-facebook"></i></a>
                                </li>
                                <li><a href=""><i class="fa fa-twitter"></i></a>
                                </li>
                                <li><a href=""><i class="fa fa-google"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END CONTACT -->



    <section class="copyright">
        <h2></h2>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="copy_right_text">
                    <!-- COPYRIGHT TEXT -->
                        <p>Startup Ukraine &copy; 2017.</p>        
                        <p>За підтримки <a href="https://vladlan91.github.io/"> Lean Consulting</a></p>
                    </div>
                     
                </div>

                <div class="col-md-6">
                    <div class="scroll_top">
                        <a href="#HOME"><i class="fa fa-angle-up"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
     <script src="js/jquery-1.11.2.min.js"></script>
     <script src="js/jquery.main.js"></script>
     <script src="js/jquery.parallax-1.1.3.js"></script>
     <script src="js/jquery.prettyPhoto.js"></script>
     <script src="js/owl.carousel.js"></script>
     <script src="js/wow.min.js"></script>
     <script src="js/waypoints.min.js"></script>
     <script src="js/jquery.counterup.min.js"></script>
     <script src="js/script.js"></script>
     <script src="js/rok.js"></script>
     <script src="js/bootstrap.min.js"></script>
</body>
</html>
