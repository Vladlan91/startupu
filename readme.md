# STARTAP UKRAINE
В то время как онлайн - образование становится весьма актуальна в мире, ее уровень в Украине совсем низкий. Мы уверены, что будущее за интерактивным обучением. STARTUP UKRAINE - родился в сочетании игровой и социальной составляющей, а основной изюминкой стала возможность на базе нашей платформы создать собственный стартап, объединив тем самым приятное с полезным.
# ИСПОЛЬЗУМЫЕ ТЕХНОЛОГИИ
##### Laravel PHP Framework
[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

##### Laravel Socialite

##### Intervention Image
[![Build Status](https://travis-ci.org/Intervention/image.png?branch=master)](https://travis-ci.org/Intervention/image)



##Инструкция к запуску проекта:
- Скачайте **Apache** [Wamp](http://www.wampserver.com/en/) или [MAMP](https://www.mamp.info/en/downloads/) или [XAMPP](https://www.apachefriends.org/download.html)
- Cоздайте локальную базу данных  `имя вашой базы данных` с кодировкой utf8_general_ci 
- В файлe `.env` внесите изменения:
```php
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT='MySQL Port - в настройках Apache'
DB_DATABASE='имя вашой базы данных'
DB_USERNAME='имя пользеватиля'
DB_PASSWORD='пароль вашой базы данных'
```
- Скачайте [Composer](https://getcomposer.org/)
- Откройте консоль в корне каталога проекта и введите:
- `composer install`
- `php artisan migrate`

#####Теперь вы можете получить доступ к своему проекту по адресу localhost:8888 :)







#Laravel PHP Framework
[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)


Laravel — бесплатный веб-фреймворк с открытым кодом, предназначенный для разработки с использованием архитектурной модели MVC (англ. Model View Controller — модель-представление-контроллер). Laravel выпущен под лицензией MIT. Исходный код проекта размещается на GitHub, документация [Laravel ru](https://laravel.ru/docs/v5/).
### Установка Laravel

Laravel использует [Composer](https://getcomposer.org/) для управления зависимостями. Поэтому сначала установите [Composer](https://getcomposer.org/), а затем Laravel.


Сначала загрузите установщик Laravel с помощью [Composer](https://getcomposer.org/).
```php
composer global require "laravel/installer"
```
### Введение

Laravel Socialite предоставляет выразительный, плавный интерфейс для аутентификации OAuth с Facebook, Twitter, Google, LinkedIn, GitHub и Bitbucket. Он обрабатывает почти весь код социальной аутентификации, который вы боитесь писать.

**Не принимает новые адаптеры.**

**Если вы используете Laravel 5.3 или ниже, используйте [Socialite 2.0](https://github.com/laravel/socialite/tree/2.0).**

Адаптеры для других платформ перечислены на [Socialite Providers](https://socialiteproviders.github.io/) website.

## License

Laravel Socialite - это программное обеспечение с открытым исходным кодом, лицензированное под [MIT license](http://opensource.org/licenses/MIT)

## Официальная документация

Чтобы начать работу с Socialite, используйте [Composer](https://getcomposer.org/) для добавления пакета в зависимости от вашего проекта:
    composer require laravel/socialite

### Конфигурация

После установки библиотеки Socialite зарегистрируйте `Laravel \ Socialite \ SocialiteServiceProvider` в файле конфигурации` config / app.php`:
```php
'providers' => [
    // Other service providers...

    Laravel\Socialite\SocialiteServiceProvider::class,
],
```

Кроме того, добавьте фасад `Socialite` в массив` aliases` в файле конфигурации `app`:

```php
'Socialite' => Laravel\Socialite\Facades\Socialite::class,
```

You will also need to add credentials for the OAuth services your application utilizes. These credentials should be placed in your `config/services.php` configuration file, and should use the key `facebook`, `twitter`, `linkedin`, `google`, `github` or `bitbucket`, depending on the providers your application requires. For example:
```php
'github' => [
    'client_id' => 'your-github-app-id',
    'client_secret' => 'your-github-app-secret',
    'redirect' => 'http://your-callback-url',
],
```
### Basic Usage
Затем вы готовы аутентифицировать пользователей! Вам понадобятся два маршрута: один для перенаправления пользователя провайдеру OAuth, а другой для получения обратного вызова от провайдера после аутентификации. Мы будем обращаться к  Socialite, используя фасад `Socialite`:

```php
<?php

namespace App\Http\Controllers\Auth;

use Socialite;

class LoginController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('github')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('github')->user();

        // $user->token;
    }
}
```

Метод `redirect` позаботится о том, чтобы отправить пользователя поставщику OAuth, в то время как метод `user` будет читать входящий запрос и получать информацию от пользователя. Перед перенаправлением пользователя вы также можете добавить дополнительные «области» запроса, используя метод `scopes`. Этот метод объединит все существующие области с теми, которые вы поставляете:

```php
return Socialite::driver('github')
            ->scopes(['scope1', 'scope2'])->redirect();
```

Вы можете переписать все существующие области с помощью метода `setScopes`:

```php
return Socialite::driver('github')
            ->setScopes(['scope1', 'scope2'])->redirect();
```

Конечно, вам нужно будет определить маршруты к вашим методам контроллера:

```php
Route::get('login/github', 'Auth\LoginController@redirectToProvider');
Route::get('login/github/callback', 'Auth\LoginController@handleProviderCallback');
```

Ряд провайдеров - OAuth поддерживают необязательные параметры в запросе на перенаправление. Чтобы включить любые необязательные параметры в запрос, вызовите метод `with` с ассоциативным массивом:

```php
return Socialite::driver('google')
            ->with(['hd' => 'example.com'])->redirect();
```

Используя методы `with` будьте осторожны, чтобы не передавать любые зарезервированные ключевые слова, такие как` state` или `response_type`.

#### Аутентификация без сохранения состояния

Метод `stateless` может использоваться для отключения проверки состояния сеанса. Это полезно при добавлении социальной аутентификации в API:

```php
return Socialite::driver('google')->stateless()->user();
```


#### Получение сведений о пользователе

Если у вас есть пользовательский экземпляр, вы можете получить еще несколько сведений о пользователе:

```php
$user = Socialite::driver('github')->user();

// OAuth Two Providers
$token = $user->token;
$refreshToken = $user->refreshToken; // not always provided
$expiresIn = $user->expiresIn;

// OAuth One Providers
$token = $user->token;
$tokenSecret = $user->tokenSecret;

// All Providers
$user->getId();
$user->getNickname();
$user->getName();
$user->getEmail();
$user->getAvatar();
```

#### Получение сведений о пользователе из Token

Если у вас уже есть токен доступа для пользователя, вы можете получить их данные, используя `userFromToken` метод:

```php
$user = Socialite::driver('github')->userFromToken($token);
```
# Intervention Image
[![Build Status](https://travis-ci.org/Intervention/image.png?branch=master)](https://travis-ci.org/Intervention/image)


Intervention Image представляет собой библиотеку **PHP обращение к изображениям и обработку изображений**, которая обеспечивает более простой и выразительный способ создания, редактирования и компоновки изображений. Пакет включает ServiceProviders и Facades для легкой интеграции **Laravel**.


## Требования

- PHP >=5.4
- Fileinfo Extension

## Поддерживаемые библиотеки изображений

- GD Library (>=2.0)
- Imagick PHP extension (>=6.5.7)

## Старт

- [Installation](http://image.intervention.io/getting_started/installation)
- [Laravel Framework Integration](http://image.intervention.io/getting_started/installation#laravel)
- [Official Documentation](http://image.intervention.io/)

## Пример кода

```php
// open an image file
$img = Image::make('public/foo.jpg');

// resize image instance
$img->resize(320, 240);

// insert a watermark
$img->insert('public/watermark.png');

// save image in desired format
$img->save('public/bar.jpg');
```