<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Http\Requests;
use App\Team;


class TeamController extends Controller
{
    public function show_teams(){

        $teams = Team::all();

        return view('teams.teams')->withTeams($teams);

    }
    public function show_team($id){


        $team = Team::find($id);

        return view('teams.show')->withTeam($team);

    }

    public function store(Request $request)
    {
        $team = new Team();
        $team->title = $request->get('title');
        $team->body = $request->get('body');
        $team->own_id = $request->user()->id;
        $team->slug = str_slug($team->title);
        if($request->has('save'))
        {
            $team->active = 0;
            $message = 'Команда успешно сохранена';
        }
        else
        {
            $team->active = 1;
            $message = 'Команда опубликована успешно';
        }
        $team->save();
        return redirect('edit-team/'.$team->slug);
    }

    public function create(Request $request)
    {
        // если пользователь может публиковать автор или администратор
        if($request->user()->can_post())
        {
            return view('teams.create');
        }
        else
        {
            return redirect('/')->withErrors('У вас нет достаточных прав для написания поста');
        }
    }

    public function edit(Request $request,$slug)
    {
        $team = Team::where('slug',$slug)->first();
        if($team && ($request->user()->id == $team->own_id || $request->user()->is_admin()))
            return view('teams.edit')->with('team',$team);
        return redirect('/')->withErrors('у вас нет достаточных прав');
    }

    public function update(Request $request)
    {
        $team_id = $request->input('team_id');
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time().'.'.$avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300,300)->save(public_path('/uploads/avatars/'.$filename));

            $team = Team::find($team_id);
            $team->avatar=$filename;
            $team->save();
        }

        $team_id = $request->input('team_id');
        $team = Team::find($team_id);
        if($team && ($team->own_id == $request->user()->id || $request->user()->is_admin()))
        {
            $title = $request->input('title');
            $slug = str_slug($title);
            $duplicate = Team::where('slug',$slug)->first();
            if($duplicate)
            {
                if($duplicate->id != $team_id)
                {
                    return redirect('edit-team/'.$team->slug)->withErrors('Title already exists.')->withInput();
                }
                else
                {
                    $team->slug = $slug;
                }
            }
            $team->title = $title;
            $team->body = $request->input('body');
            if($request->has('save'))
            {
                $team->active = 0;
                $message = 'Post saved successfully';
                $landing = 'edit-team/'.$team->slug;
            }
            else {
                $team->active = 1;
                $message = 'Post updated successfully';
                $landing = 'edit-team/'.$team->slug;
            }
            $team->save();
            return redirect($landing)->withMessage($message);
        }
        else
        {
            return redirect('/')->withErrors('у вас нет достаточных прав');
        }
    }

        public function destroy(Request $request, $id)
    {

        $team = Team::find($id);
        if($team && ($team->own_id == $request->user()->id || $request->user()->is_admin()))
        {
            $team->delete();
            $data['message'] = 'Пост успешно удалён';
        }
        else
        {
            $data['errors'] = 'Неправильная операция. У вас нет достаточных прав';
        }
        dump($team);
        return redirect('/teams')->with($data);
    }

}
