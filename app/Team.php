<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    public function own()
    {
        return $this->belongsTo('App\User','own_id');
    }

    public function projects()
    {
        return $this->hasMany('App\Project','projects_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

}
