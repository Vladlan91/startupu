<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone');
            $table->string('facebook');
            $table->string('bitbucket');
            $table->string('linkedin');
            $table->string('github');
            $table->string('avatar')->default('default.jpg');
            $table->string('password');
            $table->enum('role',['admin','author','subscriber'])->default('author');
            $table -> integer('country_id')->unsigned()->default(1);
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table -> integer('town_id')->unsigned()->default(1);
            $table->foreign('town_id')->references('id')->on('towns')->onDelete('cascade');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
