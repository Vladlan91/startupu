<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'STARTUP-UKRAINE') }}</title>
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <link rel="stylesheet" href="{{asset('css/Pe-icon-7-stroke.css')}}">
    <!-- Font Awesome -->
    <link href="{{asset('css/font-awesome.min.css')}}"rel="stylesheet">
    <!-- PrettyPhoto -->
    <link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <title>Dashboard Template for Bootstrap</title>
    <link href="../../dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="dashboard.css" rel="stylesheet">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{asset('css/mystyle.css')}}" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-inverse navbar-fixed-top navbar-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img width="200" height="30"src="{{asset('images/logo.png')}}" alt="Logo">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-inverse navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Увійти</a></li>
                    <li><a href="{{ url('/register') }}">Зареєструватись</a></li>
                    @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="position: relative; padding-left: 50px;">
                            <img src = "/uploads/avatars/{{Auth::user()->avatar}}" style="width:32px;height:32px;float: left;border-radius: 50%;top: 10px;position: absolute;left: 10px; ">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ url('profile') }}"><i class="fa fa-btn fa-area-chart"></i>
                                   Настройки
                                </a></li>
                            <li>
                                <a href="{{ url('/user/'.Auth::id())  }}"><i class="fa fa-btn fa-save"></i>
                                    Профайл
                                </a></li>
                            <li>
                                <a href="{{ url('/new-post') }}"><i class="fa fa-btn fa-cloud-upload"></i>
                                    Добавить пост
                                </a></li>
                            <li>
                                <a href="{{ url('/new-project') }}"><i class="fa fa-btn fa-briefcase"></i>
                                    Cоздать проект
                                </a></li>
                            <li>
                                <a href="{{ url('/new-team') }}"><i class="fa fa-btn fa-coffee"></i>
                                    Cоздать команду
                                </a></li>
                            <li>
                                <a href="{{ url('/logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fa fa-btn fa-sign-out"></i>

                                    Вийти
                                </a></li>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                            </li>
                        </ul>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
     @yield('navbar')
     @yield('content')
</div>

<!-- Scripts -->
<script src="/js/app.js"></script>
</body>
</html>