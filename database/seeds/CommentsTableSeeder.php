<?php

use Illuminate\Database\Seeder;
use App\Comment;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Comment::create(['on_post'=>'1','from_user'=>'1','body'=>'Орут']);
        Comment::create(['on_post'=>'1','from_user'=>'2','body'=>'Очень очень круто']);
        Comment::create(['on_post'=>'2','from_user'=>'4','body'=>'Очень смешно']);
        Comment::create(['on_post'=>'2','from_user'=>'3','body'=>'Cмешно']);
        Comment::create(['on_post'=>'3','from_user'=>'2','body'=>'Очень круто']);
        Comment::create(['on_post'=>'3','from_user'=>'1','body'=>'Очень!']);
        Comment::create(['on_post'=>'3','from_user'=>'2','body'=>'круто!']);
    }
}
