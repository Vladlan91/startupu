<?php

use Illuminate\Database\Seeder;
use App\Job;

class JobesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Job::create(['name'=>'Не вказано']);
        Job::create(['name'=>'IT']);
        Job::create(['name'=>'Consulting']);
        Job::create(['name'=>'HR']);
        Job::create(['name'=>'PR']);
    }
}
