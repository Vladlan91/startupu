<?php

use Illuminate\Database\Seeder;
use App\TeamGroup;

class TeamGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            TeamGroup::create(['user_id'=>'1','team_id'=>'2']);
            TeamGroup::create(['user_id'=>'2','team_id'=>'2']);
            TeamGroup::create(['user_id'=>'3','team_id'=>'1']);
            TeamGroup::create(['user_id'=>'1','team_id'=>'3']);
            TeamGroup::create(['user_id'=>'2','team_id'=>'3']);

        }
    }
}
