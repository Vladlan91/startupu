@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div class="row">
    <div class="row placeholders">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
            </div>
        </div>
    </div>
</div>
@foreach ($users as $user) {
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div style="width: 100%; background-color: #111c26; height: 20px" >
            <img src = "/uploads/avatars/{{$user->avatar}}" style="width:50px;height:50px;float: left;border-radius: 50%;margin-right: 25px; ">
            </div>
            <li class="list-group-item" style="z-index: -1">
                Дата реестрации - {{$user->created_at->format('M d,Y \a\t h:i a') }}
            </li>
            <li class="list-group-item">
                <a href="/user/{{$user->id}}">{{$user->name}}</a>
            </li>
            <li class="list-group-item">
                Еmail - {{$user->email}}
            </li>
            <div class="panel panel-default">
                <ul class="list-group">
<!--                    <li class="list-group-item panel-body">-->
<!--                        <table class="table-padding">-->
<!--                            <style>-->
<!--                                .table-padding td{-->
<!--                                    padding: 3px 8px;-->
<!--                                }-->
<!--                            </style>-->
<!--                            <tr>-->
<!--                                <td>Всего постов</td>-->
<!--                                <td></td>-->
<!--                            </tr>-->
<!--                            <tr>-->
<!--                                <td>Опубликованные посты</td>-->
<!--                                <td></td>-->
<!--                            </tr>-->
<!--                            <tr>-->
<!--                                <td>Страна происхождения</td>-->
<!--                                <td></td>-->
<!--                            </tr>-->
<!--                        </table>-->
<!--                    </li>-->
<!--                    <li class="list-group-item">-->
<!--                        Всего комментариев-->
<!--                    </li>-->
                </ul>
            </div>
        </div>
    </div>
</div>


@endforeach
