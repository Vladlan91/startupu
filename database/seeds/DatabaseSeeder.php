<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(CountriesTableSeeder::class);
        $this->call(TownsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TeamsTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
        $this->call(JobesTableSeeder::class);
        $this->call(JobesUsersTableSeeder::class);
        $this->call(TeamGroupTableSeeder::class);

    }
}
