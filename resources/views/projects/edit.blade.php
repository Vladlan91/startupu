@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div class="row">
    <div class="row placeholders">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Редактирования проекта!</div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript">
    tinymce.init({
        selector : "textarea",
        plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
        toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    });
</script>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <img src = "/uploads/avatars/{{$project->avatar}}" style="width:150px;height:150px;float: left;margin-right: 25px; ">
            <form method="post" action='{{ url("/update-project") }}' enctype="multipart/form-data">
                <label>Update profile Image</label>
                <input type="file" name="avatar">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="submit" class="pull-right btn btn-sm btn-primary">
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <input type="hidden" name="project_id" value="{{ $project->id }}{{ old('project_id') }}">
    <div class="form-group">
        <input required="required" placeholder="Enter title here" type="text" name = "title" class="form-control" value="@if(!old('title')){{$project->title}}@endif{{ old('title') }}"/>
    </div>
    <div class="form-group">
<textarea name='body'class="form-control">
@if(!old('body'))
{!! $project->body !!}
@endif
{!! old('body') !!}
</textarea>
    </div>
    @if($project->active == '1')
    <input type="submit" name='publish' class="btn btn-success" value = "Обновить"/>
    @else
    <input type="submit" name='publish' class="btn btn-success" value = "Опубликовать"/>
    @endif
    <input type="submit" name='save' class="btn btn-default" value = "Сохранить как черновик" />
    <a href="{{  url('delete-project/'.$project->id.'?_token='.csrf_token()) }}" class="btn btn-danger">Удалить</a>
</form>
    </div>
</div>
</div>
