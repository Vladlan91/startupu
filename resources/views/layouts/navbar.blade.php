@section('navbar')
<div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav navbar-inverse nav-sidebar">
            <li class="active"><a href="/home"><i class="fa fa-btn fa-bars"></i> Головна</a></li>
              <li><a href="/projects"><i class="fa fa-btn fa-archive"></i> Проекти</a></li>
            <li><a href="/teams"><i class="fa fa-btn fa-link"></i> Команди</a></li>
          </ul>
          <ul class="nav  navbar-inverse nav-sidebar">
            <li><a href=""><i class="fa fa-btn fa-bell"></i> Мої заявки</a></li>
            <li><a href=""><i class="fa fa-btn fa-briefcase"></i> Мої проекти</a></li>
            <li><a href=""><i class="fa fa-btn fa-asterisk"></i> Мої команди</a></li>
          </ul>
          <ul class="nav  navbar-inverse nav-sidebar">
              <li><a href="/users"><i class="fa fa-btn fa-user"></i> Користувачі</a></li>
            <li><a href="/profile"><i class="fa fa-btn fa-area-chart"></i> Налаштування</a></li>
            <li><a href="/"><i class="fa fa-btn fa-rocket"></i> Startup Ukraine></li>
          </ul>
        </div>
@endsection