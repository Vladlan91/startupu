<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],


    'twitter' => [
        'client_id' => 'fq0mVqJl7TAMi2vl5xNg9mDck',
        'client_secret' => 'p5iztr705MBe83xw0PQUztnp236IQo2NEgQTrHCTXjgsayvIl2',
        'redirect' => 'http://localhost:8888/auth/twitter/callback',
    ],

    'google' => [
        'client_id' => '443360528795-4c7pk9sfotb507mpgtg4mb6ugbimvafi.apps.googleusercontent.com',
        'client_secret' => 'L_MuZzway6fTI_ZLCwXLMZvY',
        'redirect' => 'http://localhost:8888/auth/google/callback',
    ],

];
