<?php

use Illuminate\Database\Seeder;
use App\JobUser;

class JobesUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JobUser::create(['user_id'=>'1','job_id'=>'1']);
        JobUser::create(['user_id'=>'1','job_id'=>'2']);
        JobUser::create(['user_id'=>'2','job_id'=>'2']);
        JobUser::create(['user_id'=>'3','job_id'=>'1']);
        JobUser::create(['user_id'=>'3','job_id'=>'2']);
        JobUser::create(['user_id'=>'3','job_id'=>'3']);
    }
}
