<?php

use Illuminate\Database\Seeder;
use \App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//     1   DB::insert('INSERT INTO `users` (`name`,`email`,`password`) VALUES (?,?,?)',['Elina','elina@gmail.com','1234']);

//     2   DB::table('users')->insert([['name'=>"Elina",'email'=>"elina@gmail.com",'password'=>"1234"],['name'=>"Katy",'email'=>"katy@gmail.com",'password'=>"1234"],['name'=>"Kira",'email'=>"kira@gmail.com",'password'=>"1234"]]);

        User::create(['name'=>'Katy','email'=>'katyD32@gmail.com','password'=>'1234456','country_id'=>'1','town_id'=>'1']);
        User::create(['name'=>'Kira','email'=>'kiraD32@gmail.com','password'=>'1234456','country_id'=>'2','town_id'=>'2']);
        User::create(['name'=>'Sveta','email'=>'SvetaD32@gmail.com','password'=>'1234456','country_id'=>'3','town_id'=>'3']);
        User::create(['name'=>'Lina','email'=>'linaD32@gmail.com','password'=>'1234456','country_id'=>'2','town_id'=>'2']);
        User::create(['name'=>'Alina','email'=>'alinaD32@gmail.com','password'=>'1234456','country_id'=>'1','town_id'=>'3']);
    }
}
