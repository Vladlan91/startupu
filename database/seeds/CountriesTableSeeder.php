<?php

use Illuminate\Database\Seeder;
use App\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            Country::create(['name'=>'Не вказано']);
            Country::create(['name'=>'Україна']);
            Country::create(['name'=>'Молдова']);
            Country::create(['name'=>'Польша']);
            Country::create(['name'=>'Білорусь']);

        }
    }
}
