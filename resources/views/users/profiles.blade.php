@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div class="row">
    <div class="row placeholders">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Новости</div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <img src = "/uploads/avatars/{{$user->avatar}}" style="width:150px;height:150px;float: left;border-radius: 50%;margin-right: 25px; ">
            <h2>{{$user-> name}}'s profile</h2>
            <div class="panel-heading"> Еmail - {{$user->email}}</div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3>История</h3></div>

    <ul class="list-group">
        <li class="list-group-item">
            Joined on {{$user->created_at->format('M d,Y \a\t h:i a') }}
        </li>
        <li class="list-group-item panel-body">
            <table class="table-padding">
                <style>
                    .table-padding td{
                        padding: 3px 8px;
                    }
                </style>
                <tr>
                    <td>Всего постов</td>
                    <td> {{$posts_count}}</td>
                    @if($author && $posts_count)
                    <td><a href="{{ url('/my-all-posts')}}">Показать все</a></td>
                    @endif
                </tr>
                <tr>
                    <td>Опубликованные посты</td>
                    <td>{{$posts_active_count}}</td>
                    @if($posts_active_count)
                    <td><a href="{{ url('/user/'.$user->id.'/posts')}}">Показать все</a></td>
                    @endif
                </tr>
                <tr>
                    <td>Постов в Черновиках</td>
                    <td>{{$posts_draft_count}}</td>
                    @if($author && $posts_draft_count)
                    <td><a href="{{ url('my-drafts')}}">Показать все</a></td>
                    @endif
                </tr>
                <tr>
                    <td>Страна происхождения</td>
                    <td >{{$user->country->name}}</td>
                </tr>
                <tr>
                    <td>Город</td>
                    <td>{{$user->town->name}}</td>
                </tr>
                <tr>
                    <td class="fa fa-btn fa-facebook fa-2x"></td>
                    @if(!empty($user->facebook[0]))
                    <td><a href="{{$user->facebook}}">facebook</td>
                    @else
                    <td>ссылка не указана</td>
                    @endif
                </tr>
                <tr>
                    <td class="fa fa-btn fa-linkedin fa-2x" ></td>
                    @if(!empty($user->linkedin[0]))
                    <td><a href="{{$user->linkedin}}">linkedin</td>
                    @else
                    <td>ссылка не указана</td>
                    @endif
                </tr>
                <tr>
                    <td class="fa fa-btn fa-bitbucket fa-2x"></td>
                    @if(!empty($user->bitbucket[0]))
                    <td><a href="{{$user->bitbucket}}">bitbucket</td>
                    @else
                    <td>ссылка не указана</td>
                    @endif
                </tr>
                <tr>
                    <td class="fa fa-btn fa-github fa-2x"></td>
                    @if(!empty($user->github[0]))
                    <td><a href="{{$user->github}}">github</td>
                    @else
                    <td>ссылка не указана</td>
                    @endif
                </tr>
                <tr>
                    <td>Номер телефона</td>
                    <td>{{$user->phone}}</td>
                </tr>
            </table>
        </li>
        <li class="list-group-item">
            Всего комментариев {{$comments_count}}
        </li>
    </ul>
</div>
<div class="panel panel-default">
    <div class="panel-heading"><h3>Последние посты</h3></div>
    <div class="panel-body">
        @if(!empty($latest_posts[0]))
        @foreach($latest_posts as $latest_post)
        <p>
            <strong><a href="{{ url('/'.$latest_post->slug) }}">{{ $latest_post->title }}</a></strong>
            <span class="well-sm">От {{ $latest_post->created_at->format('M d,Y \a\t h:i a') }}</span>
        </p>
        @endforeach
        @else
        <p>У {{$user->name}} нет ещё постов.</p>
        @endif
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading"><h3>Profit SKILLS</h3></div>
    <div class="panel-body">
        @if(!empty($jobes[0]))
        @foreach($jobes as $job)
        <p>

            <span class="well-sm">{{ $job->name}}</span>
        </p>
        @endforeach
        @else
        <p>У {{$user->name}} нет ещё навыков.</p>
        @endif
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading"><h3>Оснаватель команд</h3></div>
    <div class="panel-body">
        @if(!empty($teams[0]))
        @foreach($teams as $team)
        <p>
            <strong><a href="{{ url('team/'.$team->id) }}">{{ $team->title }}</a></strong>
            <img src = "/uploads/avatars/{{$team->avatar}}" style="width:30px;height:30px;float: left;border-radius: 50%;margin-right: 25px; ">
        </p>
        @endforeach
        @else
        <p>У {{$user->name}} нет ещё команд.</p>
        @endif
    </div>
</div>
    <div class="panel panel-default">
        <div class="panel-heading"><h3>Учасник команд</h3></div>
        <div class="panel-body">
            @if(!empty($teams_in[0]))
            @foreach($teams_in as $team)
            <p>
                <strong><a href="{{ url('team/'.$team->id) }}">{{ $team->title }}</a></strong>
                <img src = "/uploads/avatars/{{$team->avatar}}" style="width:30px;height:30px;float: left;border-radius: 50%;margin-right: 25px; ">
            </p>
            @endforeach
            @else
            <p>У {{$user->name}} нет ещё команд.</p>
            @endif
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><h3>Оснаватель проектов</h3></div>
        <div class="panel-body">
            @if(!empty($latest_projects[0]))
            @foreach($latest_projects as $latest_project)
            <p>
                <strong><a href="{{ url('project/'.$latest_project->id) }}">{{ $latest_project->title }}</a></strong>
                <img src = "/uploads/avatars/{{$latest_project->avatar}}" style="width:30px;height:30px;float: left;border-radius: 50%;margin-right: 25px; ">
            </p>
            @endforeach
            @else
            <p>У {{$user->name}} нет ещё проектов.</p>
            @endif
        </div>
    </div>
<div class="panel panel-default">
    <div class="panel-heading"><h3>Последние комментарии</h3></div>
    <div class="list-group">
        @if(!empty($latest_comments[0]))
        @foreach($latest_comments as $latest_comment)
        <div class="list-group-item">
            <p>{{ $latest_comment->body }}</p>
            <p>On {{ $latest_comment->created_at->format('M d,Y \a\t h:i a') }}</p>
            <p>On post <a href="{{ url('/'.$latest_comment->post->slug) }}">{{ $latest_comment->post->title }}</a></p>
        </div>
        @endforeach
        @else
        <div class="list-group-item">
            <p>У {{$user->name}} нет комментариев. {{$user->name}} последние 5 комментариев будут выведены здесь</p>
        @endif
