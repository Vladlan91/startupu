@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div class="row">
    <div class="row placeholders">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Новости</div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <img src = "/uploads/avatars/{{$user->avatar}}" style="width:150px;height:150px;float: left;border-radius: 50%;margin-right: 25px; ">
            <h2>{{$user->name}}'s profile</h2>

            <form enctype="multipart/form-data" action="/profile" method="POST">
                <label>Update profile Image</label>
                <input type="file" name="avatar">
                <input type="hidden" name="_token" value="{{csrf_token()}}">



<div class="panel panel-default">
    <div class="panel-heading"><h3>Страна</h3></div>
    <div class="panel-body">

        <div class="control-group">
            <label class="control-label" for="select01">Выбирите страну</label>
            <div class="controls">
                <select name = "country_id" id="country" action="/ajax-subcat?cat_id" class="form-control" style="width: 290px" value="@if(!old('$user->country_id')){{$user->country_id}}@endif{{ old('$user->country_id') }}">
                    <option value="{{$user->country->id}}">{{$user->country->name}}</option>
                    @foreach($countries as $country)
                    <option value="{{$country->id}}">{{$country->name}}</option>
                    @endforeach
                </select>
                <input type="submit" class="pull-right btn btn-sm btn-primary" value = "Обновить">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="select01">Выбирите город</label>
            <div class="controls">
                <select name = "town_id" class="form-control" style="width: 290px" value="@if(!old('$user->town_id')){{$user->town_id}}@endif{{ old('$user->town_id') }}">
                    <option value="{{$user->town->id}}">{{$user->town->name}}</option>
                    @foreach($towns as $town)
                    <option value="{{$town->id}}">{{$town->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading"><h3>SKILLS</h3></div>
    <div class="panel-body">
        <div class="control-group">
            <label class="control-label" for="select01">Выбирите свой опыт по направлениям</label>
            <div class="controls">
                <select multiple="multiple" name = "country_id" class="form-control" style="width: 290px" value="@if(!old('$user->country_id')){{$user->country_id}}@endif{{ old('$user->country_id') }}">
                    <option>IT</option>
                    <option>PR</option>
                    <option>HR</option>
                    <option>Counsalting</option>
                    <option>Bissnes Analis</option>
                </select>
                <input type="submit" class="pull-right btn btn-sm btn-primary" value = "Обновить">
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading"><h3>Контактная информация</h3></div>
    <div class="panel-body">
        <div class="control-group">
            <label class="control-label" for="input01">Gmail</label>
            <div class="controls">
                <input  placeholder="Enter email link here" type="text" name = "title" class="form-control" value="@if(!old('$user->email')){{$user->email}}@endif{{ old('$user->email') }}"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input01">Phone</label>
            <div class="controls">
                <input  placeholder="Enter phone here" type="text" name = "phone" class="form-control" value="@if(!old('$user->phone')){{$user->phone}}@endif{{ old('$user->phone') }}"/>
            </div>
            <input type="submit" class="pull-right btn btn-sm btn-primary" value = "Обновить">
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading"><h3>Ссилки на профайл</h3></div>
    <div class="panel-body">
        <div class="control-group">
            <label class="control-label" for="input01">Facebook</label>
            <div class="controls">
                <input  placeholder="Enter facebook link here" type="text" name = "facebook" class="form-control" value="@if(!old('$user->facebook')){{$user->facebook}}@endif{{ old('$user->facebook') }}"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input01">Linkedin</label>
            <div class="controls">
                <input  placeholder="Enter linkedin link here" type="text" name = "linkedin" class="form-control" value="@if(!old('$user->linkedin')){{$user->linkedin}}@endif{{ old('$user->linkedin') }}"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input01">Bitbucket</label>
            <div class="controls">
                <input  placeholder="Enter bitbucket link here" type="text" name = "bitbucket" class="form-control" value="@if(!old('$user->bitbucket')){{$user->bitbucket}}@endif{{ old('$user->bitbucket') }}"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="input01">GitHub</label>
            <div class="controls">
                <input  placeholder="Enter github link here" type="text" name = "github" class="form-control" value="@if(!old('$user->github')){{$user->github}}@endif{{ old('$user->github') }}"/>
                </div>
            <input type="submit" class="pull-right btn btn-sm btn-primary" value = "Обновить">
        </div>
    </div>
    </div>
            </form>
</div>