<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use DB;
use Intervention\Image\Facades\Image;
use App\Http\Requests;

class ProjectController extends Controller
{

    public function index()
    {

        $projects = Project::where('active',1)->orderBy('created_at','desc')->paginate(100);
        $title = 'Последние проекты';

        return view('projects.projects')->withProjects($projects)->withTitle($title);
    }


    public function show($id)
    {
        $project = Project::where('id',$id)->first();
        if(!$project)
        {
            return redirect('/')->withErrors('запрошенная страница не найдена');
        }
        return view('projects.show')->withProject($project);
    }


    public function edit(Request $request,$id)
    {
        $project = Project::where('id',$id)->first();
        if($project && ($request->user()->id == $project->author_id || $request->user()->is_admin()))

            return view('projects.edit')->with('project',$project);

        return redirect('/')->withErrors('у вас нет достаточных прав');
    }

    public function update(Request $request)
    {
        $project_id = $request->input('project_id');
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time().'.'.$avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300,300)->save(public_path('/uploads/avatars/'.$filename));

            $project = Project::find($project_id);
            $project->avatar=$filename;
            $project->save();
        }

        $project_id = $request->input('project_id');
        $project = Project::find($project_id);
        if($project && ($project->author_id == $request->user()->id || $request->user()->is_admin()))
        {
            $title = $request->input('title');
            $slug = str_slug($title);
            $duplicate = Project::where('slug',$slug)->first();
            if($duplicate)
            {
                if($duplicate->id != $project_id)
                {
                    return redirect('edit/'.$project->slug)->withErrors('Title already exists.')->withInput();
                }
                else
                {
                    $project->slug = $slug;
                }
            }
            $project->title = $title;
            $project->body = $request->input('body');
            if($request->has('save'))
            {
                $project->active = 0;
                $message = 'Post saved successfully';
                $landing = 'edit/'.$project->id;
            }
            else {
                $project->active = 1;
                $message = 'Post updated successfully';
                $landing = 'edit/'.$project->id;
            }
            $project->save();
            return redirect($landing)->withMessage($message);
        }
        else
        {
            return redirect('/')->withErrors('у вас нет достаточных прав');
        }
    }

    public function store(Request $request)
    {
        $project = new Project();
        $project->title = $request->get('title');
        $project->body = $request->get('body');
        $project->slug = str_slug($project->title);
        $project->author_id = $request->user()->id;
        if($request->has('save'))
        {
            $project->active = 0;
            $message = 'Проект успешно сохранён';
        }
        else
        {
            $project->active = 1;
            $message = 'Проект опубликован успешно';
        }
        $project->save();
        return redirect('edit/'.$project->id)->withMessage($message);
    }

    public function create(Request $request)
    {

        if($request->user()->can_post())
        {
            return view('projects.create');
        }
        else
        {
            return redirect('/')->withErrors('У вас нет достаточных прав для написания поста');
        }
    }

    public function destroy(Request $request, $id)
    {

        $project = Project::find($id);
        if($project && ($project->author_id == $request->user()->id || $request->user()->is_admin()))
        {
            $project->delete();
            $data['message'] = 'Пост успешно удалён';
        }
        else
        {
            $data['errors'] = 'Неправильная операция. У вас нет достаточных прав';
        }
        return redirect('/home')->with($data);
    }

}
