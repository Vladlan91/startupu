@extends('layouts.app')
@extends('layouts.navbar')
@section('content')
<div class="row">
    <div class="row placeholders">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <p></p>>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">

        @if($project)

        <div class="row placeholders">
            <div class="col-md-8 col-md-offset-2">
                <div class="list-group">
                    <div class="list-group-item">
                        <h3>{{ $project->title }}
                            @if(!Auth::guest() && ($project->author_id == Auth::user()->id || Auth::user()->is_admin()))
                            @if($project->active == '1')
                            <button type="button" class="btn" style="float: right; background-color: #111c26"><a href="{{ url('edit/'.$project->id)}}">Редактировать проект</a></button>
                            @else
                            <button type="button" class="btn" style="float: right; background-color: #111c26"><a href="{{ url('edit/'.$project->id)}}">Редактировать черновик</a></button>
                            @endif
                            @endif
                        </h3>
                        <img src = "/uploads/avatars/{{$project->author->avatar}}" style="width:30px;height:30px;float: left;border-radius: 50%;margin-right: 25px; ">
                        <p>{{ $project->created_at->format('M d,Y \a\t h:i a') }} By <a href="{{ url('/user/'.$project->author_id)}}">{{ $project->author->name }}</a></p>
                    </div>
                    <div style="width: 100%; height: 400px;">
                        <img src = "/uploads/avatars/{{$project->avatar}}" style="width:100%;height:400px;margin-right: 25px; ">
                    </div>
                    <div class="list-group-item">
                        <article>
                            {!!$project->body!!}
                        </article>
                    </div>
                    <div class="list-group">
                        <a href="#" class="list-group-item">  <span><i class="pe-7s-timer"></i></span> Просмотрено <span class="badge">9</span>
                        </a>
                        <a href="#" class="list-group-item"> <span><i class="pe-7s-timer"></i></span> Подписаных <span class="badge">24</span>
                        </a>
                        <a href="#" class="list-group-item"> <span><i class="pe-7s-timer"></i></span> Лайков <span class="badge">411</span>

                        </a>
                    </div>
                </div>
            </div>
        </div>
        @endif