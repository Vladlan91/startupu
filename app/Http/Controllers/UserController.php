<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use App\Http\Requests;
use App\Post;
use App\Country;
use App\Project;
use App\Team;
use App\town;

class UserController extends Controller
{
    public function profile()
    {
        $user = Auth::user();
        $countries = Country::all();
        $towns = Town::where('country_id','=',$user->country_id)->get();
        return view('users.profile',array('user'=> Auth::user()))->withCountries($countries)->withTowns($towns);
    }
    public function update_avatar(Request $request)
    {
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time().'.'.$avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300,300)->save(public_path('/uploads/avatars/'.$filename));
            $user = Auth::user();
            $user->avatar=$filename;
        }
            $user = Auth::user();
            $usercountry= $request->input('country_id');
            $usertown= $request->input('town_id');
            $user->country_id = $usercountry;
            $user->town_id = $usertown;
            $facebook= $request->input('facebook');
            $bitbucket= $request->input('bitbucket');
            $linkedin= $request->input('linkedin');
            $github= $request->input('github');
            $user->facebook = $facebook;
            $phone= $request->input('phone');
            $user->phone = $phone;
            $user->bitbucket = $bitbucket;
            $user->linkedin = $linkedin;
            $user->github = $github;
            $user->save();
            $towns = Town::where('country_id','=',$user->country_id)->get();
            $countries = Country::all();

        return view('users.profile',array('user'=> Auth::user()))->withCountries($countries)->withTowns($towns);
    }
    public function show_users(){


          $users = User::all();
        return view('users.users')->withUsers($users);

    }

    public function user_posts($id)
    {

        $posts = Post::where('author_id',$id)->where('active',1)->orderBy('created_at','desc')->paginate(50);
        $title = User::find($id)->name;
        return view('home')->withPosts($posts)->withTitle($title);
    }

    public function user_posts_all(Request $request)
    {

        $user = $request->user();
        $posts = Post::where('author_id',$user->id)->orderBy('created_at','desc')->paginate(5);
        $title = $user->name;
        return view('home')->withPosts($posts)->withTitle($title);
    }

    public function user_posts_draft(Request $request)
    {

        $user = $request->user();
        $posts = Post::where('author_id',$user->id)->where('active',0)->orderBy('created_at','desc')->paginate(5);
        $title = $user->name;
        return view('home')->withPosts($posts)->withTitle($title);
    }

    public function profiles(Request $request, $id)
    {
        $data['user'] = User::find($id);
        if (!$data['user'])
            return redirect('/');
        if ($request -> user() && $data['user'] -> id == $request -> user() -> id) {
            $data['author'] = true;
        } else {
            $data['author'] = null;
        }
        $data['teams']=$data['user']->own_teams;
        $data['jobes']=$data['user']->jobes;
        $data['teams_in']= $data['user']->teams;
        $data['comments_count'] = $data['user'] -> comments -> count();
        $contry_id = $data['user'] -> country_id;
        $contry = Country::find($contry_id);
        $contry_name = $contry->name;
        $data['latest_projects']= $data['user'] -> projects->  where('active', '1') -> take(5);
        $data['contry_name']=$contry_name;
        $data['posts_count'] = $data['user'] -> posts -> count();
        $data['posts_active_count'] = $data['user'] -> posts -> where('active', '1') -> count();
        $data['posts_draft_count'] = $data['posts_count'] - $data['posts_active_count'];
        $data['latest_posts'] = $data['user'] -> posts -> where('active', '1') -> take(5);
        $data['latest_comments'] = $data['user'] -> comments -> take(5);

        return view('users.profiles', $data);
    }
}
